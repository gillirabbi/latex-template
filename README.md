# README #

This is a TeX template, by *Gísli Rafn Guðmundsson*, used for LaTeX written reports, documents, research papers, assignments and more.

*Hjalti Magnússon* (Adjunct Faculty at Reykjavík University) provided the basis for this template.